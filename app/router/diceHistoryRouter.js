// Khai báo thư viện express
const express = require('express');

// Import Middleware
const { diceHistoryMiddleware } = require('../middleware/diceHistoryMiddleware');


//Import  Controller
const { diceHandler, getDiceHistoryByUser, getPrizeHistoryByUser, getVoucherHistoryByUser } =
    require('../controller/diceHistoryController');

// Tạo router
const diceHistoryRouter = express.Router();

// Sử dụng middleware
diceHistoryRouter.use(diceHistoryMiddleware);


// KHAI BÁO API
//CREATE A Dice History
diceHistoryRouter.post('/devcamp-lucky-dice/dice', diceHandler);
//GET Dice History by user
diceHistoryRouter.get('/devcamp-lucky-dice/dice-history', getDiceHistoryByUser);
//get prize history by user
diceHistoryRouter.get('/devcamp-lucky-dice/prize-history', getPrizeHistoryByUser);
// get voucher history by user
diceHistoryRouter.get('/devcamp-lucky-dice/voucher-history', getVoucherHistoryByUser);

// EXPORT ROUTER
module.exports = diceHistoryRouter;